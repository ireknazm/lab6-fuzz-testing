import { calculateBonuses } from "./bonus-system.js";

describe("Bonus system tests", () => {
  beforeAll(() => console.log("Tests started"));

  test("Returns correct value for Unknown program and zero amount", () => {
    expect(calculateBonuses("Unknown", 0)).toEqual(0);
  });

  test("Returns correct value for Standard program and zero amount", () => {
    expect(calculateBonuses("Standard", 0)).toEqual(0.05);
  });

  test("Returns correct value for Premium program and zero amount", () => {
    expect(calculateBonuses("Premium", 0)).toEqual(0.1);
  });

  test("Returns correct value for Diamond program and zero amount", () => {
    expect(calculateBonuses("Diamond", 0)).toEqual(0.2);
  });

  test("Returns correct value for Premium program and 10000 amount", () => {
    expect(calculateBonuses("Premium", 10000)).toEqual(0.1 * 1.5);
  });

  test("Returns correct value for Premium program and 50000 amount", () => {
    expect(calculateBonuses("Premium", 50000)).toEqual(0.1 * 2);
  });

  test("Returns correct value for Premium program and 100000 amount", () => {
    expect(calculateBonuses("Premium", 100000)).toEqual(0.1 * 2.5);
  });

  afterAll(() => console.log("Tests Finished"));
});
